package digi.poc.library.controller;

import com.google.common.collect.Lists;
import digi.poc.library.LibraryApplication;
import digi.poc.library.entity.Book;
import digi.poc.library.repository.BookRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.Optional;


@RestController
public class LibraryController {
	private static final Logger log = LoggerFactory.getLogger(LibraryApplication.class);

	//todo If time move add service layer.
	private BookRepository bookRepository;


	@Autowired
	public void setBookRepository(BookRepository bookRepository) {
		this.bookRepository = bookRepository;
	}

	@GetMapping(value = "All/listBooks")
	public List<Book> listAllBooks() {
		return Lists.newArrayList(bookRepository.findAll());
	}

	@GetMapping(value = "/getBook")
	public Book getBook(@RequestBody final String title) {
		try {
			Optional<Book> book = bookRepository.findByTitle(title);
			if (book.isPresent()) {
				return book.get();
			} else {
				log.debug("bookRepository could not find a book with title:" + title);
				return null;
			}
		} catch (Exception e) {
			log.error("Could not get Book: " + title + "received the Following Exception:", e);
			return null;
		}

	}

	@PostMapping(value = "/addBook")
	public Book addBook(@RequestBody Book book) {
		try {
			if (!bookRepository.findByTitle(book.getTitle()).isPresent()) {
				bookRepository.save(book);
			}
		} catch (Exception e) {
			log.error("Could not save Book: " + book + "received the Following Exception:", e);
			return null;
		}
		return book;
	}

	@PostMapping(value = "/updateBook")
	public Book updateBook(@RequestBody Book book) {
		try {
			Optional<Book> bookOld = bookRepository.findByTitle(book.getTitle());
			if (bookOld.isPresent()) {
				Book updateBook = bookOld.get();
				updateBook.setTitle(book.getTitle() != null ? book.getTitle() : updateBook.getTitle());
				updateBook.setAuthor(book.getAuthor() != null ? book.getAuthor() : updateBook.getAuthor());
				updateBook.setFormat(book.getFormat() != null ? book.getFormat() : updateBook.getFormat());
				updateBook.setAvailableForCheckOut(book.getAvailableForCheckOut() != null ? book.getAvailableForCheckOut() : updateBook.getAvailableForCheckOut());
				updateBook.setDueDate(book.getDueDate() != null ? book.getDueDate() : updateBook.getDueDate());
				bookRepository.save(updateBook);
				return updateBook;
			}
			return null;
		} catch (Exception e) {
			log.error("Could not update Book: " + book + "received the Following Exception:", e);
			return new Book("NA", "NA", "NA");
		}
	}

	@DeleteMapping(value = "/deleteBook")
	public void listAllBooks(@PathVariable final long bookId) {
		bookRepository.deleteById(bookId);
	}

}

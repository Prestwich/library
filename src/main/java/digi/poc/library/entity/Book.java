package digi.poc.library.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import java.util.Date;

@Entity
public class Book {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "book_id")
	@JsonIgnore
	private Integer id;

	private String title;
	private String author;
	private String format;

	//Todo consider splitting into a new entity if time allows (technical debit.)
	@Column(name = "available")
	private Boolean availableForCheckOut;
	private Date dueDate;


	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getAuthor() {
		return author;
	}

	public void setAuthor(String author) {
		this.author = author;
	}

	public String getFormat() {
		return format;
	}

	public void setFormat(String format) {
		this.format = format;
	}

	public Boolean getAvailableForCheckOut() {
		return availableForCheckOut;
	}

	public void setAvailableForCheckOut(Boolean availableForCheckOut) {
		this.availableForCheckOut = availableForCheckOut;
	}

	public Date getDueDate() {
		return dueDate;
	}

	public void setDueDate(Date dueDate) {
		this.dueDate = dueDate;
	}

	public Book(String title, String author, String format) {
		this.title = title;
		this.author = author;
		this.format = format;
	}


	public Book() {

	}

	@Override
	public String toString() {
		return "Book{" +
				"id=" + id +
				", title='" + title + '\'' +
				", author='" + author + '\'' +
				", format='" + format + '\'' +
				", availableForCheckOut=" + availableForCheckOut +
				", dueDate=" + dueDate +
				'}';
	}
}

package digi.poc.library;

import digi.poc.library.entity.Book;
import digi.poc.library.repository.BookRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;

@SpringBootApplication
public class LibraryApplication {
	private static final Logger log = LoggerFactory.getLogger(LibraryApplication.class);

	public static void main(String[] args) {
		SpringApplication.run(LibraryApplication.class, args);
	}

	@Bean
	public CommandLineRunner demo(BookRepository repository) {
		return (args) -> {
			repository.save(new Book("Moby Dick", "Herman Melville", "audiobook"));
			repository.save(new Book("The Picture of Dorian Gray", "Oscar Wilde", "paperback"));
			repository.save(new Book("Jane Eyre", "Charlotte Brontë", "hardcover"));
			repository.save(new Book("Handmaid's Tale", "Atwood", "hardcover"));

			for (Book book : repository.findAll()) {
				log.info("The application is: " + book.toString());
			}
		};
	}

}

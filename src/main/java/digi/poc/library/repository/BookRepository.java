package digi.poc.library.repository;

import digi.poc.library.entity.Book;
import org.springframework.data.repository.CrudRepository;

import java.util.Optional;

public interface BookRepository extends CrudRepository<Book, Long> {
	Optional<Book> findByTitle(String title);
}
